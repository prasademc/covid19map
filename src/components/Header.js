import React from 'react';

import Container from 'components/Container';

const Header = () => {
  return (
    <header>
      <Container type="content">
        <p>COVID-19(Corona virus) - Global Current status</p>
      </Container>
    </header>
  );
};

export default Header;
