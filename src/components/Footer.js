import React from 'react';

import Container from 'components/Container';

const Footer = () => {
  return (
    <footer>
      <Container>
        <p>By <a href="mailto:prasademc@gmail.com">prasademc</a>, &copy; { new Date().getFullYear() } </p>
      </Container>
    </footer>
  );
};

export default Footer;
