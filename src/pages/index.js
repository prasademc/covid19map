import React from 'react';
import Helmet from 'react-helmet';
import Layout from 'components/Layout';
import Map from 'components/Map';
import axios from 'axios';
import L from 'leaflet';

const LOCATION = {
  lat: 38.9072,
  lng: -77.0369
};
const CENTER = [LOCATION.lat, LOCATION.lng];
const DEFAULT_ZOOM = 2;

const IndexPage = () => {

  /**
   * mapEffect
   * @description Fires a callback once the page renders
   * @example Here this is and example of being used to zoom in and set a popup on load
   */

  async function mapEffect({ leafletElement: map } = {}) {
    let response;

    try {
      response = await axios.get('https://corona.lmao.ninja/v2/countries');
    } catch(e) {
      console.log(`Failed to fetch countries: ${e.message}`, e);
      return;
    }

    const { data = [] } = response;
    const hasData = Array.isArray(data) && data.length > 0;

    if( !hasData ) return;

    const geoJson = {
      type: 'FeatureCollection',
      features: data.map((country = {}) => {
        const { countryInfo = {} } = country;
        const { lat, long: lng } = countryInfo;
        return {
          type: 'Feature',
          properties: {
          ...country,
          },
          geometry: {
            type: 'Point',
            coordinates: [ lng, lat ]
          }
        }
      })
    }

    function pointCountriesToLayer (feature = {}, latlng) {
      const { properties = {} } = feature;
      let updatedFormatted;
      let casesString;

      const {
        country,
        updated,
        cases,
        deaths,
        todayDeaths,
        recovered,
        todayCases,
        active,
        critical,
        tests,
        countryInfo: {
          flag
        }
      } = properties

      casesString = `${cases}`;

      if ( cases > 1000 ) {
        casesString = `${casesString.slice(0, -3)}k+`
      }

      if ( updated ) {
        updatedFormatted = new Date(updated).toLocaleString();
      }

      const html = `
        <span class="icon-marker">
          <span class="icon-marker-tooltip">
            <img width="100%" src=${flag} >
            <h2>${country}</h2>
            <ul>
              <li>Confirmed all cases: <strong>${cases}</strong></li>
              <li>Confirmed today cases: <strong>${todayCases}</strong></li>
              <li>All deaths: <strong>${deaths}</strong></li>
              <li>Today deaths: <strong>${todayDeaths}</strong></li>
              <li>Active cases: <strong>${active}</strong></li>
              <li>critical cases: <strong>${critical}</strong></li>
              <li>Recovered: <strong>${recovered}</strong></li>
              <li>Number of tests: <strong>${tests}</strong></li>
              <li>Last Update: <strong>${updatedFormatted}</strong></li>
            </ul>
          </span>
          ${ casesString }
        </span>
      `;

      return L.marker( latlng, {
        icon: L.divIcon({
          className: 'icon',
          html
        }),
        riseOnHover: true
      });
    }

    const geoJsonLayers = new L.GeoJSON(geoJson, {
      pointToLayer: pointCountriesToLayer
    });

    geoJsonLayers.addTo(map);
  }

  const mapSettings = {
    center: CENTER,
    defaultBaseMap: 'OpenStreetMap',
    zoom: DEFAULT_ZOOM,
    mapEffect
  };

  return (
    <Layout pageName="home">
      <Helmet>
        <title>Home Page</title>
      </Helmet>

      <Map {...mapSettings} />
    </Layout>
  );
};

export default IndexPage;
